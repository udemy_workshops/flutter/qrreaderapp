import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

import '../models/scan_model.dart';
export '../models/scan_model.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> _initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'scansdb.db');
    return await openDatabase(
      path,
      version: 1,
      onOpen: (Database db) async {},
      onCreate: (Database db, int version) async {
        await db.execute(
          'CREATE TABLE scans('
          'id INTEGER PRIMARY KEY,'
          'tipo TEXT,'
          'valor TEXT'
          ')',
        );
      },
    );
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initDB();
    }
    return _database;
  }

  // crear registros
  nuevoScanRaw(ScanModel nuevoScan) async {
    final db = await database;
    final res = await db.rawInsert("INSERT INTO scans(id,tipo,valor)"
        "VALUES(${nuevoScan.id},'${nuevoScan.tipo}','${nuevoScan.valor}')");
    return res;
  }

  nuevoScan(ScanModel nuevoScan) async {
    final db = await database;
    final res = await db.insert('scans', nuevoScan.toJSON());
    return res;
  }

  // obtener datos
  Future<ScanModel> getScanId(int id) async {
    final db = await database;
    final res = await db.query('scans', where: 'id=?', whereArgs: [id]);
    return res.isNotEmpty ? ScanModel.fromJSON(res.first) : null;
  }

  Future<List<ScanModel>> getTodosScans() async {
    final db = await database;
    final res = await db.query('scans');
    List<ScanModel> list = res.isNotEmpty
        ? res.map((item) => ScanModel.fromJSON(item)).toList()
        : [];
    return list;
  }

  Future<List<ScanModel>> getScansPorTipo(String tipo) async {
    final db = await database;
    // final res = await db.query('scans', where: 'tipo=?', whereArgs: [tipo]);
    final res = await db.rawQuery("SELECT * FROM scans WHERE tipo = '$tipo'");
    List<ScanModel> list = res.isNotEmpty
        ? res.map((item) => ScanModel.fromJSON(item)).toList()
        : [];
    return list;
  }

  // actualizar registros
  Future<int> updateScan(ScanModel nuevoScan) async {
    final db = await database;
    final res = await db.update('scans', nuevoScan.toJSON(),
        where: 'id=?', whereArgs: [nuevoScan.id]);
    return res;
  }

  // eliminar registros
  Future<int> deleteScan(int id) async {
    final db = await database;
    final res = await db.delete('scans', where: 'id=?', whereArgs: [id]);
    return res;
  }

  Future<int> deleteAll() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM scans');
    return res;
  }
}
