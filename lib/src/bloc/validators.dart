import 'dart:async';

import '../models/scan_model.dart';

class Validators {
  final validarGEO =
      StreamTransformer<List<ScanModel>, List<ScanModel>>.fromHandlers(
    handleData: (List<ScanModel> scans, EventSink<List<ScanModel>> sink) {
      final List<ScanModel> geoScans =
          scans.where((s) => s.tipo == 'geo').toList();
      sink.add(geoScans);
    },
  );

  final validarHTTP =
      StreamTransformer<List<ScanModel>, List<ScanModel>>.fromHandlers(
    handleData: (List<ScanModel> scans, EventSink<List<ScanModel>> sink) {
      final List<ScanModel> httpScans =
          scans.where((s) => s.tipo == 'http').toList();
      sink.add(httpScans);
    },
  );
}
