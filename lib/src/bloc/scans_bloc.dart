import 'dart:async';

import 'validators.dart';
import '../providers/db_provider.dart';

class ScansBloc with Validators {
  static final ScansBloc _singleton = new ScansBloc._internal();
  final _streamController = StreamController<List<ScanModel>>.broadcast();

  factory ScansBloc() {
    return _singleton;
  }

  dispose() {
    _streamController?.close();
  }

  Stream<List<ScanModel>> get streamGEO => _streamController.stream.transform(validarGEO);
  Stream<List<ScanModel>> get streamHTTP => _streamController.stream.transform(validarHTTP);

  ScansBloc._internal() {
    // obtener los scans de la base de datos
    obtenerScans();
  }

  obtenerScans() async {
    _streamController.sink.add(await DBProvider.db.getTodosScans());
  }

  agregarScan(ScanModel scan) async {
    await DBProvider.db.nuevoScan(scan);
    obtenerScans();
  }

  borrarScan(int id) async {
    await DBProvider.db.deleteScan(id);
    obtenerScans();
  }

  borrarScansTodos() async {
    await DBProvider.db.deleteAll();
    obtenerScans();
  }
}
