import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import '../models/scan_model.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPage createState() => _MapPage();
}

class _MapPage extends State<MapPage> {
  final MapController _mapCtrl = new MapController();

  String _tipoMapa = 'streets';

  @override
  Widget build(BuildContext context) {
    final ScanModel scan = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Coordenadas QR'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.my_location,
            ),
            onPressed: () {
              _mapCtrl.move(scan.getLatLng(), 15);
            },
          )
        ],
      ),
      body: _crearFlutterMap(scan),
      floatingActionButton: _crearBotonFlotante(context),
    );
  }

  Widget _crearBotonFlotante(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        // streets, dark, light, outdoors, satellite
        setState(() {
          switch (_tipoMapa) {
            case 'streets':
              _tipoMapa = 'dark';
              break;
            case 'dark':
              _tipoMapa = 'light';
              break;
            case 'light':
              _tipoMapa = 'outdoors';
              break;
            case 'outdoors':
              _tipoMapa = 'satellite';
              break;
            case 'satellite':
              _tipoMapa = 'streets';
              break;
            default:
              _tipoMapa = 'streets';
              break;
          }
        });
      },
      backgroundColor: Theme.of(context).primaryColor,
      child: Icon(
        Icons.repeat,
      ),
    );
  }

  Widget _crearFlutterMap(ScanModel scan) {
    return FlutterMap(
      mapController: _mapCtrl,
      options: MapOptions(
        center: scan.getLatLng(),
        zoom: 15,
      ),
      layers: [
        _crearMapa(),
        _crearMarcadores(scan),
      ],
    );
  }

  LayerOptions _crearMapa() {
    return TileLayerOptions(
      urlTemplate: 'https://api.mapbox.com/v4/'
          '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: <String, String>{
        'accessToken':
            'pk.eyJ1IjoibGNhcnJlbm8iLCJhIjoiY2s0eWNmajBuMGFiYzNrbjN2MHd6N2MwOSJ9.pJp9w1FJcfhQjUqAAAln0w',
        'id': 'mapbox.$_tipoMapa',
        // streets, dark, light, outdoors, satellite
      },
    );
  }

  LayerOptions _crearMarcadores(ScanModel scan) {
    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          width: 35.0,
          height: 35.0,
          point: scan.getLatLng(),
          builder: (BuildContext context) {
            return Container(
              // color: Colors.red,
              child: Icon(
                Icons.location_on,
                size: 35.0,
                color: Theme.of(context).primaryColor,
              ),
            );
          },
        ),
      ],
    );
  }
}
